<?php

namespace App\Controller;

use App\Repository\BookRepository;
use App\Repository\CommentRepository;
use App\Entity\Book;
use App\Entity\User;
use App\Entity\Comment;
use App\Entity\Rating;
use App\Tools\FileTools;
use App\Repository\TypeRepository;
use App\Repository\AuthorRepository;
use App\Repository\RatingRepository;

class BookController extends Controller
{
    public function route(): void
    {
        try {
            if (isset($_GET['action'])) {
                switch ($_GET['action']) {
                    case 'show':
                        $this->show();
                        break;
                    case 'add':
                        $this->add();
                        break;
                    case 'edit':
                        $this->edit();
                        break;
                    case 'delete':
                        $this->delete();
                        break;
                    case 'list':
                        $this->list();
                        break;
                    default:
                        throw new \Exception("Cette action n'existe pas : " . $_GET['action']);
                        break;
                }
            } else {
                throw new \Exception("Aucune action détectée");
            }
        } catch (\Exception $e) {
            $this->render('errors/default', [
                'error' => $e->getMessage()
            ]);
        }
    }

    /*
    Exemple d'appel depuis l'url
        ?controller=book&action=show&id=1
    */
    protected function show(): void
    {
        $errors = [];

        try {
            if (isset($_GET['id'])) {

                $id = (int)$_GET['id'];
                // Charger le livre par un appel au repository findOneById
                $bookRepository = new BookRepository();
                $book = $bookRepository->findOneById($id);

                if ($book) {
                    $commentRepository = new CommentRepository();
                    if (isset($_POST['saveComment'])) {
                        if (!User::isLogged()) {
                            throw new \Exception("Accès refusé");
                        }
                        $comment = Comment::createAndHydrate($_POST);
                        $errors = $comment->validate();
                        if (empty($errors)) {
                            $isCreated = $commentRepository->persist($comment);
                            if (!$isCreated) {
                                $errors[] = "Votre commentaire n'a pas été créé";
                            }
                        }
                    }

                    $rating = new Rating();
                    $ratingRepository = new RatingRepository();
                    $rating = $ratingRepository->findOneByBookIdAndUserId($book->getId(), User::getCurrentUserId());
                    if (isset($_POST["saveRating"])) {
                        if (!User::isLogged()) {
                            throw new \Exception("Accès refusé");
                        }
                        $rating->hydrate($_POST);
                        $errors = $rating->validate();
                        if (empty($errors)) {
                            $isCreated = $ratingRepository->persist($rating);
                            if (!$isCreated) {
                                $errors[] = "Votre évaluation n'a pas été créé";
                            }
                        }
                    }
                    $ratings = $ratingRepository->findAverageByBookId($book->getId());

                    $comments = $commentRepository->findAllByBookId($book->getId());

                    $this->render('book/show', [
                        'book' => $book,
                        'comments' => $comments,
                        'newComment' => '',
                        'rating' => $rating,
                        'averageRate' => $ratings,
                        'errors' => $errors,
                    ]);
                } else {
                    $this->render('errors/default', [
                        'error' => 'Livre introuvable'
                    ]);
                }
            } else {
                throw new \Exception("L'id est manquant en paramètre");
            }
        } catch (\Exception $e) {
            $this->render('errors/default', [
                'error' => $e->getMessage()
            ]);
        }
    }

    protected function add(): void
    {
        $this->add_edit();
    }

    protected function edit(): void
    {
        try {
            if (isset($_GET['id'])) {
                $this->add_edit((int)$_GET['id']);
            } else {
                throw new \Exception("L'id est manquant en paramètre");
            }
        } catch (\Exception $e) {
            $this->render('errors/default', [
                'error' => $e->getMessage()
            ]);
        }
    }

    protected function add_edit($id = null): void
    {

        try {
            // Cette action est réservé aux admin
            if (!User::isLogged() || !User::isAdmin()) {
                throw new \Exception("Accès refusé");
            }
            $bookRepository = new BookRepository();
            $errors = [];
            // Si on a pas d'id on est dans le cas d'une création
            if (is_null($id)) {
                $book = new Book();
            } else {
                // Si on a un id, il faut récupérer le livre
                $book = $bookRepository->findOneById($id);
                if (!$book) {
                    throw new \Exception("Le livre n'existe pas");
                }
            }

            // @todo Récupération des types


            // @todo Récupération des auteurs


            if (isset($_POST['saveBook'])) {
                //@todo envoyer les données post à la méthode hydrate de l'objet $book


                //@todo appeler la méthode validate de l'objet book pour récupérer les erreurs (titre vide)


                // Si pas d'erreur on peut traiter l'upload de fichier
                if (empty($errors)) {
                    $fileErrors = [];
                    // On lance l'upload de fichier
                    if (isset($_FILES['file']['tmp_name']) && $_FILES['file']['tmp_name'] !== '') {
                        //@todo appeler la méthode static uploadImage de la classe FileTools et stocker le résultat dans $res

                        if (empty($res['errors'])) {
                            //@todo décommenter cette ligne
                            //$book->setImage($res['fileName']);
                        } else {
                            $fileErrors = $res['errors'];
                        }
                    }
                    if (empty($fileErrors)) {
                        // @todo si pas d'erreur alors on appelle persit de bookRepository en passant $book


                        // @todo On redirige vers la page du livre (avec header location)

                    } else {
                        $errors = array_merge($errors, $fileErrors);
                    }
                }
            }

            $this->render('book/add_edit', [
                'book' => $book,
                'types' => '',
                'authors' => '',
                'pageTitle' => 'Ajouter un livre',
                'errors' => ''
            ]);
        } catch (\Exception $e) {
            $this->render('errors/default', [
                'error' => $e->getMessage()
            ]);
        }
    }

    protected function list(): void
    {
        $bookRepository = new BookRepository;

        // On récupère la page courante, si page de page on met à 1
        if (isset($_GET['page'])) {
            $page = (int)$_GET['page'];
        } else {
            $page = 1;
        }

        $books = $bookRepository->findAll(_ITEM_PER_PAGE_, $page);
        $totalPage = ceil($bookRepository->count() / _ITEM_PER_PAGE_);

        $this->render('book/list', [
            'books' => $books,
            'totalPages' => $totalPage,
            'page' => $page,
        ]);
    }


    protected function delete(): void
    {
        try {
            // Cette action est réservé aux admin
            if (!User::isLogged() || !User::isAdmin()) {
                throw new \Exception("Accès refusé");
            }

            if (!isset($_GET['id'])) {
                throw new \Exception("L'id est manquant en paramètre");
            }
            $bookRepository = new BookRepository();

            $id = (int)$_GET['id'];

            $book = $bookRepository->findOneById($id);

            if (!$book) {
                throw new \Exception("Le livre n'existe pas");
            }
            if ($bookRepository->removeById($id)) {
                // On redirige vers la liste de livre
                header('location: index.php?controller=book&action=list&alert=delete_confirm');
            } else {
                throw new \Exception("Une erreur est survenue l'ors de la suppression");
            }

        } catch (\Exception $e) {
            $this->render('errors/default', [
                'error' => $e->getMessage()
            ]);
        }
    }
}
