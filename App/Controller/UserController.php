<?php

namespace App\Controller;

use App\Repository\UserRepository;
use App\Entity\User;


class UserController extends Controller
{
    public function route(): void
    {
        try {
            if (isset($_GET['action'])) {
                switch ($_GET['action']) {
                    case 'register':
                        $this->register();
                        break;
                    case 'delete':
                        // Appeler méthode delete()
                        break;
                    default:
                        throw new \Exception("Cette action n'existe pas : " . $_GET['action']);
                        break;
                }
            } else {
                throw new \Exception("Aucune action détectée");
            }
        } catch (\Exception $e) {
            $this->render('errors/default', [
                'error' => $e->getMessage()
            ]);
        }
    }

    protected function register(): void
    {
        try {
            $errors = [];
            $user = new User();
            $isCreated = false;

            if (isset($_POST['saveUser'])) {
                $userRepository = new UserRepository();
                $user->hydrate($_POST);
                $errors = $user->validate();
                if (empty($errors)) {
                    $isCreated = $userRepository->persist($user);
                }
            }

            $this->render('user/add_edit', [
                'user' => $isCreated,
                'pageTitle' => 'Inscription',
                'errors' => $errors
            ]);

        } catch (\Exception $e) {
            $this->render('errors/default', [
                'error' => $e->getMessage()
            ]);
        }

    }


}
